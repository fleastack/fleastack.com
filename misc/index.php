<html>
<link rel="stylesheet" href="/fleastack.css" type="text/css">
<head>
	<title>FleaStack - Miscellaneous</title>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-26550900-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body>
	<font face="sans-serif" size="5"><b>fleastack.com</b></font>
	<br>
	<font face="sans-serif" size="2">a repository of miscellany</font>
	<br><br><br>
	<div align="justify"><font face="sans-serif" size="2">Miscellaneous stuff that doesn't fit anywhere else.</font></div>
	<br>
	<font face="sans-serif" size="3">Kid Quotes</font> (Coming Soon!)
	<br>
	<dd>
		Things our kids have said and done.
	</dd>
	<br><br>
	<a href="./books.php"><font face="sans-serif" size="3">Books I've Read</font></a>
	<br>
	<dd>
		A list of books I've read.
	</dd>
<!--# include virtual="/footer.inc" -->
</body>
</html>