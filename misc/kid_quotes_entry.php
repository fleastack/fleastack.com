<?php
include("db_connect.inc");
?>
<html>
<link rel="stylesheet" href="/fleastack.css" type="text/css">
<head>
	<title>FleaStack - Kid Quotes Entry</title>
	<script>
		function autoTab(currField,nextField) {
			if (currField.getAttribute&&currField.value.length==currField.getAttribute("maxlength"))
				nextField.focus();
		}
	</script>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-26550900-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body>
	<font face="sans-serif" size="5"><b>fleastack.com</b></font>
	<br />
	<font face="sans-serif" size="2">a repository of miscellany</font>
	<br /><br /><br />
	<font size="3">Things Our Kids Have Said (& Done)</font></div>
	<br /><br />
<?php
	if(!empty($_REQUEST["submit"])) {
		$year = $_REQUEST["year"];
		$whosaidit = 0;
		foreach($_REQUEST["whosaidit"] as $value) {
			$whosaidit += $value;
		}
		$quote_entry_query = "INSERT INTO kid_quotes (whosaidit,date,quote) VALUES ($whosaidit,'".$_REQUEST["year"]."-".$_REQUEST["month"]."-".$_REQUEST["day"]."','".$_REQUEST["quote"]."')";
#		print $quote_entry_query;
		if(mysql_query($quote_entry_query)) {
			print "\tQuote added successfully!<br /><br />\n";
		}
	} else {
		$year = date("Y");
	}
?>
	<form method="post">
		<table>
			<tr>
				<td>Who Said It:</td>
				<td><input type="checkbox" name="whosaidit[]" value="1"> Ian<br /><input type="checkbox" name="whosaidit[]" value="2"> Hadassah<br /><input type="checkbox" name="whosaidit[]" value="4"> Leah</td>
			</tr>
			<tr>
				<td>Date:</td>
				<td><input type="text" name="month" size="2" maxlength="2" onKeyUp="autoTab(this,this.form.day)">/<input type="text" name="day" size="2" maxlength="2" onKeyUp="autoTab(this,this.form.year)">/<input type="text" name="year" value="<?=$year;?>" size="4" maxlength="4" onKeyUp="autoTab(this,this.form.quote)"></td>
			</tr>
			<tr>
				<td>Quote:</td>
				<td><textarea name="quote" cols="40" rows="10"></textarea></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" name="submit" value="Submit Quote"></td>
			</tr>
		</table>
	</form>
<?php
include("../footer.inc");
?>
</body>
</html>