<?php
include("db_connect.inc");
function print_a($array) {
	print "<pre>\n";
	print_r($array);
	print "</pre>\n";
}
?>
<html>
<link rel="stylesheet" href="/fleastack.css" type="text/css">
<head>
	<title>FleaStack - Kid Quotes</title>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-26550900-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body>
	<font face="sans-serif" size="5"><b>fleastack.com</b></font>
	<br />
	<font face="sans-serif" size="2">a repository of miscellany</font>
	<br /><br /><br />
<?php
	$whosaidit_names = array(1 => "Ian", 2 => "Hadassah", 3 => "Leah");
	$quotes_count_query = "SELECT whosaidit FROM kid_quotes ORDER BY date DESC";
	$quotes_count = mysql_query($quotes_count_query);
	while($qcdata = mysql_fetch_assoc($quotes_count)) {
		++$total_quotes;
		$whosaidit_numbers = explode(",",$qcdata["whosaidit"]);
		foreach($whosaidit_numbers as $number) {
			++$whosaidit_counts[$number];
		}
	}
	$output = "";
	if(count($_REQUEST["whosaidit"])) {
		// print_a($_REQUEST["whosaidit"]);
		$whosaidit_count = 0;
		$whosaidit_condition = "";
		foreach($_REQUEST["whosaidit"] as $value) {
			if($whosaidit_count > 0) {
				$whosaidit_condition .= " ".$_REQUEST["whosaidit_condition"]." ";
			}
			$whosaidit_condition .= " whosaidit LIKE '%".$value."%'";
			++$whosaidit_count;
		}
/*
		print $whosaidit."<br />";
		print $whosaidit_condition;
*/
		$quotes_query = "SELECT * FROM kid_quotes WHERE $whosaidit_condition ORDER BY date DESC";
	} else {
		$quotes_query = "SELECT * FROM kid_quotes ORDER BY date DESC";
	}
	$quotes = mysql_query($quotes_query);
	// print_a($whosaidit_counts);
?>
	<font size="3">Things Our Kids Have Said (&amp; Done)</font>
	<br /><br />
	<form>
	<table style="border: none; padding: 0px;">
<?php
		foreach($whosaidit_names as $wsi_number => $wsi_name) {
			print "\t\t<tr>\n";
			print "\t\t\t<td>\n";
			print "\t\t\t\t<span class=\"kid_quotes_heading\"><input type=\"checkbox\" name=\"whosaidit[]\" value=\"$wsi_number\"";
			if(!empty($_REQUEST["whosaidit"])) {
				foreach($_REQUEST["whosaidit"] as $rwsi_number) {
					if($rwsi_number == $wsi_number) {
						print " checked";
					}
				}
			}
			print "> <a href=\"kid_quotes.php?whosaidit[]=$wsi_number\"> $wsi_name ($whosaidit_counts[$wsi_number])</a></span>\n";
			print "\t\t\t</td>\n";
			print "\t\t</tr>\n";
		}
?>
		<tr>
			<td>
				<span class="kid_quotes_heading"><a href="kid_quotes.php">All (<?php print $total_quotes;?>)</a></span>
			</td>
		</tr>
		<tr>
			<td>
				<select name="whosaidit_condition">
					<option value="OR">OR</option>
					<option value="AND">AND</option>
				</select>
				<input type="submit" value="Sort" />
			</td>
		</tr>
	</table>
	</form>
	<table width="500" border="0"><tr><td width="15">&nbsp;</td><td>
	<table border="0" cellspacing="0" cellpadding="5" width="100%">
<?php
		// $quotes_query = "SELECT * FROM kid_quotes ORDER BY date DESC";
		$i = 1;
		$q = mysql_num_rows($quotes);
		while($kq_data = mysql_fetch_assoc($quotes)) {
			print "\t\t<tr>\n";
			print "\t\t\t<td>\n";
			print "\t\t\t\t<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\n";
			print "\t\t\t\t\t<tr>\n";
			print "\t\t\t\t\t\t<td align=\"left\"><span class=\"kid_quotes_heading\">".date("F j, Y",strtotime($kq_data["date"]))."</span></td>\n";
			print "\t\t\t\t\t</tr>\n";
			print "\t\t\t\t\t<tr>\n";
			print "\t\t\t\t\t\t<td align=\"justify\">".$kq_data["quote"]."</td>\n";
			print "\t\t\t\t\t</tr>\n";
			print "\t\t\t\t\t<tr>\n";
			print "\t\t\t\t\t\t<td align=\"right\" style=\"line-height: 24px; \">";
			$whosaidit_numbers = explode(",",$kq_data["whosaidit"]);
			$wn_count = 0;
			foreach($whosaidit_numbers as $number) {
				if($wn_count > 0) {
					print " | ";
				}
				print "$whosaidit_names[$number]";
				++$wn_count;
			}
			if($i != $q) {
				print "<br /><hr class=\"kid_quotes\" />";
			}
			print "</td>\n";
			print "\t\t\t\t\t</tr>\n";
			print "\t\t\t\t</table>\n";
			print "\t\t\t</td>\n";
			print "\t\t</tr>\n";
			++$i;
		}
?>
	</table>
	</td></tr>
	</table>
<?php
include("../footer.inc");
?>
</body>
</html>