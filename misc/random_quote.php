<?php
include("db_connect.inc");
function print_a($array) {
	print "<pre>\n";
	print_r($array);
	print "</pre>\n";
}
?>
<html>
<link rel="stylesheet" href="/fleastack.css" type="text/css">
<head>
	<title>FleaStack - Random Kid Quote</title>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-26550900-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body>
	<font face="sans-serif" size="5"><b>fleastack.com</b></font>
	<br />
	<font face="sans-serif" size="2">a repository of miscellany</font>
	<br /><br /><br />
	<font size="3">Things Our Kids Have Said (&amp; Done)</font>
	<br /><br />
	<table width="500" style="border: 1px solid; padding: 0px; text-align: center;">
<?php
	$whosaidit_names = array(1 => "Ian", 2 => "Hadassah", 3 => "Leah");
	$random_row = mysql_fetch_assoc(mysql_query("SELECT * FROM kid_quotes ORDER BY rand() LIMIT 1"));
	// print_a($random_row);
	print "\t\t<tr>\n";
	print "\t\t\t<td>\n";
	print "\t\t\t\t<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\n";
	print "\t\t\t\t\t<tr>\n";
	print "\t\t\t\t\t\t<td align=\"left\"><span class=\"kid_quotes_heading\">".date("F j, Y",strtotime($random_row["date"]))."</span></td>\n";
	print "\t\t\t\t\t</tr>\n";
	print "\t\t\t\t\t<tr>\n";
	print "\t\t\t\t\t\t<td align=\"justify\">".$random_row["quote"]."</td>\n";
	print "\t\t\t\t\t</tr>\n";
/*
	print "\t\t\t\t\t<tr>\n";
	print "\t\t\t\t\t\t<td align=\"right\" style=\"line-height: 24px; \">";
	$whosaidit_numbers = explode(",",$random_row["whosaidit"]);
	$wn_count = 0;
	foreach($whosaidit_numbers as $number) {
		if($wn_count > 0) {
			print " | ";
		}
		print "$whosaidit_names[$number]";
		++$wn_count;
	}
	print "</td>\n";
	print "\t\t\t\t\t</tr>\n";
*/
	print "\t\t\t\t</table>\n";
	print "\t\t\t</td>\n";
	print "\t\t</tr>\n";
?>
</table>
</body>
</html>