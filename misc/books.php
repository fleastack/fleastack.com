<?php
	$pages_array = array();
?>
<html>
<link rel="stylesheet" href="/fleastack.css" type="text/css">
<head>
	<title>FleaStack - Books I've Read</title>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-26550900-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body>
	<font face="sans-serif" size="5"><b>fleastack.com</b></font>
	<br>
	<font face="sans-serif" size="2">a repository of miscellany</font>
	<br><br><br>
	<font size="3">Books I've Read (Completely)</font></div>
	<br /><br />
<?php
$i = 0;
?>
	<table class="bordered">
		<tr>
			<th></th><th>Book Title</th><th>Author</th><th>Pages</th>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Catch Me if You Can</i></td><td>Frank W. Abagnale, Jr., with Stan Redding</td><td><?php $pages_array[] = 253;?>253</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>How I Paid for College: A Novel of Sex, Theft, Friendship, and Musical Theater</i></td><td>Marc Acito</td><td><?php $pages_array[] = 278;?>278</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Attack of the Theater People</i></td><td>Marc Acito</td><td><?php $pages_array[] = 358;?>358</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Hitchhiker's Guide to the Galaxy</i></td><td>Douglas Adams</td><td><?php $pages_array[] = 271;?>271</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Restaurant at the End of the Universe</i></td><td>Douglas Adams</td><td><?php $pages_array[] = 250;?>250</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Life, the Universe and Everything</i></td><td>Douglas Adams</td><td><?php $pages_array[] = 227;?>227</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>So Long, and Thanks for All the Fish</i></td><td>Douglas Adams</td><td><?php $pages_array[] = 204;?>204</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Mostly Harmless</i></td><td>Douglas Adams</td><td><?php $pages_array[] = 277;?>277</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Open: An Autobiography</i></td><td>Andre Agassi</td><td><?php $pages_array[] = 385;?>385</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>I'm Not Really Here</i></td><td>Tim Allen</td><td><?php $pages_array[] = 255;?>255</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Carlisle vs. Army: Jim Thorpe, Dwight Eisenhower, Pop Warner, and the Forgotten Story of Football's Greatest Battle</i></td><td>Lars Anderson</td><td><?php $pages_array[] = 349;?>349</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Front Porch Prophet</i></td><td>Raymond L. Atkins</td><td><?php $pages_array[] = 308;?>308</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Jonathan Livingston Seagull</i></td><td>Richard Bach</td><td><?php $pages_array[] = 93;?>93</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Dave Barry Does Japan</i></td><td>Dave Barry</td><td><?php $pages_array[] = 210;?>210</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Dave Barry's Complete Guide to Guys</i></td><td>Dave Barry</td><td><?php $pages_array[] = 189;?>189</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Common Sense</i></td><td>Glenn Beck</td><td><?php $pages_array[] = 174;?>174</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Da Vinci Code</i></td><td>Dan Brown</td><td><?php $pages_array[] = 467;?>467</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Children of the Mind</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 349;?>349</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Empire</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 351;?>351</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Ender in Exile</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 384;?>384</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Ender's Game</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 324;?>324</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Ender's Shadow</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 379;?>379</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>First Meetings in the Enderverse</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 208;?>208</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Hidden Empire</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 335;?>335</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Shadow of the Giant</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 367;?>367</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Shadow of the Hegemon</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 365;?>365</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Shadow Puppets</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 348;?>348</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Speaker for the Dead</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 416;?>416</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Treason</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 275;?>275</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>A War of Gifts: An Ender Story</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 126;?>126</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Xenocide</i></td><td>Orson Scott Card</td><td><?php $pages_array[] = 394;?>394</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Shallows: What the Internet is Doing to Our Brains</i></td><td>Nicholas Carr</td><td><?php $pages_array[] = 276;?>276</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Alice's Adventures in Wonderland</i></td><td>Lewis Carroll</td><td><?php $pages_array[] = 86;?>86</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The A.B.C. Murders</i></td><td>Agatha Christie</td><td><?php $pages_array[] = 256;?>256</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>2001: A Space Odyssey</i></td><td>Arthur C. Clarke</td><td><?php $pages_array[] = 221;?>221</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>2010: Odyssey Two</i></td><td>Arthur C. Clarke</td><td><?php $pages_array[] = 303;?>303</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>2061: Odyssey Three</i></td><td>Arthur C. Clarke</td><td><?php $pages_array[] = 254;?>254</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Andromeda Strain</i></td><td>Michael Crichton</td><td><?php $pages_array[] = 295;?>295</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>A Case of Need</i></td><td>Michael Crichton (as Jeffrey Hudson)</td><td><?php $pages_array[] =309 ;?>309</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Disclosure</i></td><td>Michael Crichton</td><td><?php $pages_array[] = 397;?>397</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Eaters of the Dead</i></td><td>Michael Crichton</td><td><?php $pages_array[] = 199;?>199</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Great Train Robbery</i></td><td>Michael Crichton</td><td><?php $pages_array[] = 266;?>266</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Jurassic Park</i></td><td>Michael Crichton</td><td><?php $pages_array[] = 399;?>399</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Lost World</i></td><td>Michael Crichton</td><td><?php $pages_array[] = 393;?>393</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Rising Sun</i></td><td>Michael Crichton</td><td><?php $pages_array[] = 355;?>355</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Sphere</i></td><td>Michael Crichton</td><td><?php $pages_array[] = 385;?>385</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Terminal Man</i></td><td>Michael Crichton</td><td><?php $pages_array[] = 247;?>247</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Charlie and the Chocolate Factory</i></td><td>Roald Dahl</td><td><?php $pages_array[] = 161;?>161</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Having Our Say: The Delany Sisters' First 100 Years</i></td><td>Sarah L. and A. Elizabeth Delany with Amy Hill Hearth</td><td><?php $pages_array[] = 210;?>210</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Great Expectations</i></td><td>Charles Dickens</td><td><?php $pages_array[] = 448;?>448</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>World's Fair</i></td><td>E. L. Doctorow</td><td><?php $pages_array[] = 288;?>288</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Nickel and Dimed: On (Not) Getting By in America</i></td><td>Barbara Ehrenreich</td><td><?php $pages_array[] = 221;?>221</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Neverending Story</i></td><td>Michael Ende</td><td><?php $pages_array[] = 396;?>396</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>American On Purpose: The Improbable Adventures of an Unlikely Patriot</i></td><td>Craig Ferguson</td><td><?php $pages_array[] = 268;?>268</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Between the Bridge and the River: A Novel</i></td><td>Craig Ferguson</td><td><?php $pages_array[] = 329;?>329</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Outliers: the Story of Success</i></td><td>Malcolm Gladwell</td><td><?php $pages_array[] = 309;?>309</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Lord of the Flies</i></td><td>William Goulding</td><td><?php $pages_array[] = 256;?>256</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Abraham Lincoln: Vampire Hunter</i></td><td>Seth Grahame-Smith</td><td><?php $pages_array[] = 337;?>337</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Stupid History: Tales of Stupidity, Strangeness, and Mythconceptions Throughout the Ages</i></td><td>Leland Gregory</td><td><?php $pages_array[] = 267;?>267</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Client</i></td><td>John Grisham</td><td><?php $pages_array[] = 421;?>421</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Curious Incident of the Dog in the Night-Time</i></td><td>Mark Haddon</td><td><?php $pages_array[] = 226;?>226</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Plainsong</i></td><td>Kent Haruf</td><td><?php $pages_array[] = 301;?>301</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>A Brief History of Time: From the Big Bang to Black Holes</i></td><td>Stephen W. Hawking</td><td><?php $pages_array[] = 198;?>198</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>A Murder for Her Majesty</i></td><td>Beth Hilgartner</td><td><?php $pages_array[] = 241;?>241</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Seabiscuit: An American Legend</i></td><td>Laura Hillenbrand</td><td><?php $pages_array[] = 399;?>399</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>On the Road</i></td><td>Jack Kerouac</td><td><?php $pages_array[] = 310;?>310</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Into Thin Air: A Personal Account of the Mount Everest Disaster</i></td><td>Jon Krakauer</td><td><?php $pages_array[] = 407;?>407</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Physics of Star Trek</i></td><td>Lawrence M. Krauss</td><td><?php $pages_array[] = 188;?>188</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Don’t Make Me Think! : A Common Sense Approach to Web Usability</i></td><td>Steve Krug</td><td><?php $pages_array[] = 201;?>201</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Left Behind: A Novel of the Earth's Last Days</i></td><td>Tim LaHaye & Jerry B. Jenkins</td><td><?php $pages_array[] = 468;?>468</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Tribulation Force: The Continuing Drama of Those Left Behind</i></td><td>Tim LaHaye & Jerry B. Jenkins</td><td><?php $pages_array[] = 450;?>450</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Nicolae: The Rise of Antichrist</i></td><td>Tim LaHaye & Jerry B. Jenkins</td><td><?php $pages_array[] = 415;?>415</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Soul Harvest: The World Takes Sides</i></td><td>Tim LaHaye & Jerry B. Jenkins</td><td><?php $pages_array[] = 426;?>426</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Stuff White People Like: the Definitive Guide to the Unique Taste of Millions</i></td><td>Christian Lander</td><td><?php $pages_array[] = 174;?>174</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>To Kill a Mockingbird</i></td><td>Harper Lee</td><td><?php $pages_array[] = 296;?>296</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>A Wrinkle In Time</i></td><td>Madeleine L'Engle</td><td><?php $pages_array[] = 211;?>211</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Chronicles of Narnia: The Lion, the Witch and the Wardrobe - A Story for Children</i></td><td>C. S. Lewis</td><td><?php $pages_array[] = 154;?>154</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Chronicles of Narnia: Prince Caspian - The Return to Narnia</i></td><td>C. S. Lewis</td><td><?php $pages_array[] = 194;?>194</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Chronicles of Narnia: The Voyage of the Dawn Treader</i></td><td>C. S. Lewis</td><td><?php $pages_array[] = 219;?>219</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Chronicles of Narnia: The Silver Chair</i></td><td>C. S. Lewis</td><td><?php $pages_array[] = 208;?>208</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Chronicles of Narnia: The Horse and His Boy</i></td><td>C. S. Lewis</td><td><?php $pages_array[] = 191;?>191</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Chronicles of Narnia: The Magician's Nephew</i></td><td>C. S. Lewis</td><td><?php $pages_array[] = 167;?>167</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Chronicles of Narnia: The Last Battle</i></td><td>C. S. Lewis</td><td><?php $pages_array[] = 174;?>174</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Mere Christianity</i></td><td>C. S. Lewis</td><td><?php $pages_array[] = 176;?>176</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Big Short: Inside the Doomsday Machine</i></td><td>Michaele Lewis</td><td><?php $pages_array[] = 266;?>266</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Small World: An Academic Romance</i></td><td>David Lodge</td><td><?php $pages_array[] = 338;?>338</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Greatest Salesman in the World</i></td><td>Og Mandino</td><td><?php $pages_array[] = 108;?>108</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Born Standing Up</i></td><td>Steve Martin</td><td><?php $pages_array[] = 209;?>209</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>An Object of Beauty</i></td><td>Steve Martin</td><td><?php $pages_array[] = 295;?>295</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Picasso at the Lapin Agile and Other Plays</i></td><td>Steve Martin</td><td><?php $pages_array[] = 150;?>150</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Pleasure of My Company</i></td><td>Steve Martin</td><td><?php $pages_array[] = 163;?>163</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Pure Drivel</i></td><td>Steve Martin</td><td><?php $pages_array[] = 104;?>104</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Shopgirl</i></td><td>Steve Martin</td><td><?php $pages_array[] = 130;?>130</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Soft Apocalypse</i></td><td>Will McIntosh</td><td><?php $pages_array[] = 256;?>256</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>1421: The Year China Discovered America</i></td><td>Gavin Menzies</td><td><?php $pages_array[] = 522;?>522</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Blood of Heaven</i></td><td>Bill Myers</td><td><?php $pages_array[] = 326;?>326</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Eragon</i></td><td>Christopher Paolini</td><td><?php $pages_array[] = 472;?>472</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Eldest</i></td><td>Christopher Paolini</td><td><?php $pages_array[] = 681;?>681</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Brisingr</i></td><td>Christopher Paolini</td><td><?php $pages_array[] = 763;?>763</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Inheritance</i></td><td>Christopher Paolini</td><td><?php $pages_array[] = 860;?>860</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Cry, the Beloved Country</i></td><td>Alan Paton</td><td><?php $pages_array[] = 283;?>283</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Kiss the Girls</i></td><td>James Patterson</td><td><?php $pages_array[] = 496;?>496</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Private Screening</i></td><td>Richard North Patterson</td><td><?php $pages_array[] = 322;?>322</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Gospel According to the Simpsons</i></td><td>Mark I. Pinsky</td><td><?php $pages_array[] = 164;?>164</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Hot Zone</i></td><td>Richard Preston</td><td><?php $pages_array[] = 300;?>300</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Babyhood</i></td><td>Paul Reiser</td><td><?php $pages_array[] = 205;?>205</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Couplehood</i></td><td>Paul Reiser</td><td><?php $pages_array[] = 145;?>145</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Harry Potter and the Sorcerer's Stone</i></td><td>J. K. Rowling</td><td><?php $pages_array[] = 309;?>309</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Harry Potter and the Chamber of Secrets</i></td><td>J. K. Rowling</td><td><?php $pages_array[] = 341;?>341</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Harry Potter and the Prisoner of Azkaban</i></td><td>J. K. Rowling</td><td><?php $pages_array[] = 435;?>435</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Harry Potter and the Goblet of Fire</i></td><td>J. K. Rowling</td><td><?php $pages_array[] = 734;?>734</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Harry Potter and the Order of the Phoenix</i></td><td>J. K. Rowling</td><td><?php $pages_array[] = 870;?>870</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Harry Potter and the Half-Blood Prince</i></td><td>J. K. Rowling</td><td><?php $pages_array[] = 652;?>652</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Harry Potter and the Deathly Hallows</i></td><td>J. K. Rowling</td><td><?php $pages_array[] = 759;?>759</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Contact</i></td><td>Carl Sagan</td><td><?php $pages_array[] = 432;?>432</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Pastoralia: Stories</i></td><td>George Saunders</td><td><?php $pages_array[] = 188;?>188</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>SeinLanguage</i></td><td>Jerry Seinfeld</td><td><?php $pages_array[] = 180;?>180</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Julius Caesar</i></td><td>William Shakespeare</td><td><?php $pages_array[] = 317;?>317</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>A Midsummer Night's Dream</i></td><td>William Shakespeare</td><td><?php $pages_array[] = 176;?>176</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>A Light in the Attic</i></td><td>Shel Silverstein</td><td><?php $pages_array[] = 167;?>167</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Where the Sidewalk Ends</i></td><td>Shel Silverstein</td><td><?php $pages_array[] = 166;?>166</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Singularity</i></td><td>William Sleator</td><td><?php $pages_array[] = 170;?>170</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>I'm Good Enough, I'm Smart Enough, and Doggone It, People Like Me!: Daily Affirmations With Stuart Smalley</i></td><td>Stuart Smalley (Al Franken)</td><td><?php $pages_array[] = 352;?>352</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Longitude</i></td><td>Dava Sobel</td><td><?php $pages_array[] = 216;?>216</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Grapes of Wrath</i></td><td>John Steinbeck</td><td><?php $pages_array[] = 581;?>581</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Of Mice and Men</i></td><td>John Steinbeck</td><td><?php $pages_array[] = 186;?>186</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Ella Fitzgerald: A Twentieth-Century Life</i></td><td>Tanya Lee Stone</td><td><?php $pages_array[] = 203;?>203</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Case for Christ: A Journalist's Personal Investigation of the Evidence for Jesus</i></td><td>Lee Strobel</td><td><?php $pages_array[] = 303;?>303</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Case for Faith: A Journalist Investigates the Toughest Objections to Christianity</i></td><td>Lee Strobel</td><td><?php $pages_array[] = 300;?>300</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Vienna Prelude</i></td><td>Brock & Bodie Thoene</td><td><?php $pages_array[] = 410;?>410</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Prague Counterpoint</i></td><td>Brock & Bodie Thoene</td><td><?php $pages_array[] = 406;?>406</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Munich Signature</i></td><td>Brock & Bodie Thoene</td><td><?php $pages_array[] = 424;?>424</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Jerusalem Interlude</i></td><td>Brock & Bodie Thoene</td><td><?php $pages_array[] = 439;?>439</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Danzig Passage</i></td><td>Brock & Bodie Thoene</td><td><?php $pages_array[] = 442;?>442</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Warsaw Requiem</i></td><td>Brock & Bodie Thoene</td><td><?php $pages_array[] = 520;?>520</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Hobbit: or There and Back Again</i></td><td>J.R.R. Tolkien</td><td><?php $pages_array[] = 310;?>310</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Lord of the Rings: The Fellowship of the Ring</i></td><td>J.R.R. Tolkien</td><td><?php $pages_array[] = 423;?>423</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Lord of the Rings: The Two Towers</i></td><td>J.R.R. Tolkien</td><td><?php $pages_array[] = 352;?>352</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Lord of the Rings: The Return of the King</i></td><td>J.R.R. Tolkien</td><td><?php $pages_array[] = 440;?>440</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Bagombo Snuff Box</i></td><td>Kurt Vonnegut</td><td><?php $pages_array[] = 295;?>295</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Breakfast of Champions, or, Goodbye, Blue Monday!</i></td><td>Kurt Vonnegut</td><td><?php $pages_array[] = 295;?>295</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>God Bless You, Dr. Kevorkian</i></td><td>Kurt Vonnegut</td><td><?php $pages_array[] = 79;?>79</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Hocus Pocus</i></td><td>Kurt Vonnegut</td><td><?php $pages_array[] = 302;?>302</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Mother Night</i></td><td>Kurt Vonnegut</td><td><?php $pages_array[] = 202;?>202</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Slaughterhouse-Five</i></td><td>Kurt Vonnegut</td><td><?php $pages_array[] = 186;?>186</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Timequake</i></td><td>Kurt Vonnegut</td><td><?php $pages_array[] = 219;?>219</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>Welcome to the Monkey House</i></td><td>Kurt Vonnegut</td><td><?php $pages_array[] = 298;?>298</td>
		</tr>
		<tr>
			<td><?php echo ++$i;?>.</td><td><i>The Shack</i></td><td>William P. Young</td><td><?php $pages_array[] = 266;?>266</td>
		</tr>
<?php
		$total_page_count = array_sum($pages_array);
?>
		<tr>
			<th colspan="3">Total Pages</th><th><?php echo number_format($total_page_count);?></th>
		</tr>
	</table>
<?php
include("../footer.inc");
?>
</body>
</html>