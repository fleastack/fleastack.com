<html>
<link rel="stylesheet" href="/fleastack.css" type="text/css">
<head>
	<title>FleaStack - Faith</title>
</head>
<body>
	<font face="sans-serif" size="5"><b>fleastack.com</b></font>
	<br>
	<font face="sans-serif" size="2">a repository of miscellany</font>
	<br><br><br>
	<div align="justify"><font face="sans-serif" size="2">Various writings by myself and others.</font></div>
	<br>
	<a href="./small_group/"><font face="sans-serif" size="3">Small Group Resources</font></a>
	<br>
	<dd>
		Stuff for small group.
	</dd>
	<br>
	<a href="./content/BibleBooksChart.pdf"><font face="sans-serif" size="3">Books of the Bible</font></a>
	<br>
	<dd>
		<font size="1">A chart outlining all 66 books of the Bible, their settings, and major themes.</font>
	</dd>
	<br>
<?php
include("../footer.inc");
?>
</body>
</html>