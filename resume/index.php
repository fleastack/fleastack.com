<html>
<link rel="stylesheet" href="../fleastack.css" type="text/css">
<head>
	<title>FleaStack - R&eacute;sum&eacute;s</title>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-26550900-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body>
	<font face="sans-serif" size="5"><b>fleastack.com</b></font>
	<br />
	<font face="sans-serif" size="2">a repository of miscellany</font>
	<br /><br /><br />
	<div align="justify"><font face="sans-serif" size="2">My R&eacute;sum&eacute;(s)</font></div>
	<br>
	<a href="./technology.php"><font face="sans-serif" size="3">Technology</font></a>
	<br />
	<ul>
		<li class="nav_li">
			Technology Work History
		</li>
	</ul>
	<a href="./trumpet.php"><font face="sans-serif" size="3">Trumpet</font></a>
	<br />
	<ul>
		<li class="nav_li">
			Notable Trumpet Work and Experience.
		</li>
	</ul>
<?php
include("../footer.inc");
?>
</body>
</html>
