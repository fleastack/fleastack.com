<?php
	$pages_array = array();
	$this_file_path = $_SERVER["SCRIPT_NAME"];
	$file_path_parts = explode('/', $this_file_path);
	$this_file = $file_path_parts[count($file_path_parts)-1];
	$modified_date = filemtime($this_file);
	$career_length = time() - strtotime('1997-09-01');
	$career_length_years = date("Y", time()) - 1998;
	$early_career = strtotime('1998-01-01') - strtotime('1997-09-01');
	$early_career_cell = floor(($early_career/$career_length)*800);
	$remainder = time() - strtotime((date("Y", time())).'-01-01');
	$year_cell = floor(((($career_length - $early_career - $remainder)/($career_length_years))/$career_length)*800);
	$remainder_cell = 796 - (($career_length_years * $year_cell) + $early_career_cell);
	$truman_length = strtotime('2000-12-01') - strtotime('1997-09-01');
	$monsanto_length = strtotime('2001-09-01') - strtotime('2001-01-01');
	$irrelevant_length = strtotime('2002-09-01') - strtotime('2001-09-01');
	$uhaul_length = strtotime('2004-11-01') - strtotime('2002-09-01');
	$sluh_length = strtotime('2012-07-01') - strtotime('2004-11-01');
	$caci_length = strtotime('2013-05-01') - strtotime('2012-07-01');
	$ang_length = time() - strtotime('2009-08-17');
	$truman_width = floor(($truman_length/$career_length)*800);
	$monsanto_width = floor(($monsanto_length/$career_length)*800);
	$irrelevant_width = floor(($irrelevant_length/$career_length)*800);
	$uhaul_width = floor(($uhaul_length/$career_length)*800);
	$sluh_width = floor(($sluh_length/$career_length)*800);
	$caci_width = floor(($caci_length/$career_length)*800);
	$ang_width = floor(($ang_length/$career_length)*800);
	$centurylink_width = (790 - $caci_width - $sluh_width - $uhaul_width - $irrelevant_width - $monsanto_width - $truman_width);
?>
<html>
<link rel="stylesheet" href="../fleastack.css" type="text/css">
<head>
	<title>FleaStack - Technology R&eacute;sum&eacute;</title>
	<!--Load the AJAX API-->
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript">
		// Load the Visualization API and the piechart package.
		google.load('visualization', '1.0', {'packages':['corechart']});

		// Set a callback to run when the Google Visualization API is loaded.
		google.setOnLoadCallback(drawChart);

		// Callback that creates and populates a data table, 
		// instantiates the pie chart, passes in the data and
		// draws it.
		function drawChart() {
			// Create the data table.
			var data = new google.visualization.DataTable();
			data.addColumn('string', 'Language');
			data.addColumn('number', 'Proficiency');
			data.addRows([
				['PHP', 8.5],
				['HTML/CSS/JS/jQuery', 8],
				['MySQL', 8.5],
				['Python', 4.5],
				['Perl', 3],
				['Shell Scripting', 7]
			]);
			// Set chart options
			var options = {'title':'Programming Language Proficiency (1 to 10)',
				'legend':'none',
				'width':360,
				'height':200};
			// Instantiate and draw our chart, passing in some options.
			var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
			chart.draw(data, options);
		}
	</script>
	<script type="text/javascript">
	<!--
		function toggle_visibility(id) {
			var e = document.getElementById(id);
			var e_img = document.getElementById(id+'_img');
			if(e.style.display == 'block') {
				e.style.display = 'none';
				e_img.src = '../images/resume/right_arrow.png';
			} else {
				e.style.display = 'block';
				e_img.src = '../images/resume/down_arrow.png';
			}
		}
	//-->
	</script>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-26550900-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body>
	<font face="sans-serif" size="5"><b>fleastack.com</b></font>
	<br /><br /><br />
	<div class="mainbody">
		<font size="3">Technology R&eacute;sum&eacute;</font>
		<br /><br />
		<a href="javascript:toggle_visibility('experience')" style="color: black;"><span class="resume_heading">Experience</span></a>
		<div id="experience" class="resume_division" style="display: visible;">
			<table style="width: 800px; padding: 0px; margin: 0px; border: 0px; border-collapse: collapse; border-spacing: 0px;">
				<tbody style="width: 800px;">
				<tr>
					<td>
						<table style="width: 800px; padding: 0px; margin: 0px; border: 0px; border-collapse: collapse; border-spacing: 0px;">
							<tbody style="width: 800px;">
							<tr>
								<td width="<?php echo (796 - $ang_width) - 2;?>"></td>
								<td bgcolor="#3F6393" width="<?php echo $ang_width;?>"><a href="javascript:toggle_visibility('mo_ang')"><img src="../images/resume/transparent.png" width="<?php echo $ang_width;?>" height="20" border="0" title="Missouri Air National Guard" alt="Missouri Air National Guard"></a></td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table style="width: 800px; padding: 0px; margin: 0px; border: 0px; border-collapse: collapse; border-spacing: 0px;">
							<tbody style="width: 800px;">
							<tr>
								<td bgcolor="#451068" width="<?php echo $truman_width - 2;?>"><a href="javascript:toggle_visibility('truman_its')"><img src="../images/resume/transparent.png" width="<?php echo $truman_width - 2;?>" height="20" border="0" title="Truman IT Services" alt="Truman IT Services"></a></td>
								<!-- Original Monsanto color was #00360E - too difficult to distinguish from the Truman purple -->
								<td bgcolor="#00550E" width="<?php echo $monsanto_width - 2;?>"><a href="javascript:toggle_visibility('monsanto')"><img src="../images/resume/transparent.png" width="<?php echo $monsanto_width - 2;?>" height="20" border="0" title="Monsanto Corp." alt="Monsanto Corp."></a></td>
								<td bgcolor="#CCCCCC" width="<?php echo $irrelevant_width - 2;?>"><img src="../images/resume/transparent.png" width="<?php echo $irrelevant_width - 2;?>" height="20" border="0" title="Irrelevant Experience" alt="Irrelevant Experience"></td>
								<td bgcolor="#E63F27" width="<?php echo $uhaul_width - 2;?>"><a href="javascript:toggle_visibility('u-haul')"><img src="../images/resume/transparent.png" width="<?php echo $uhaul_width - 2;?>" height="20" border="0" title="U-Haul International" alt="U-Haul International"></a></td>
								<td bgcolor="#00337f" width="<?php echo $sluh_width - 2;?>"><a href="javascript:toggle_visibility('sluh')"><img src="../images/resume/transparent.png" width="<?php echo $sluh_width - 2;?>" height="20" border="0" title="St. Louis University High" alt="St. Louis University High"></a></td>
								<td bgcolor="#000000" width="<?php echo $caci_width - 2;?>" style="border-left:1px solid gray; border-right:1px solid gray"><a href="javascript:toggle_visibility('caci')"><img src="../images/resume/transparent.png" width="<?php echo $caci_width - 2;?>" height="20" border="0" title="CACI, Inc." alt="CACI, Inc."></a></td>
								<td bgcolor="#00853F" width="<?php echo $centurylink_width - 2;?>"><a href="javascript:toggle_visibility('centurylink')"><img src="../images/resume/transparent.png" width="<?php echo $centurylink_width - 2;?>" height="20" border="0" title="Savvis" alt="CenturyLink"></a></td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td>
							<table style="width: 800px; border: 0px; padding: 0px; margin: 0px; border-collapse: collapse; font-size: 10px;">
								<tr>
<?php
									$year = 1997;
									print "\t\t\t\t\t\t\t\t\t".'<td width="'.$early_career_cell.'"> </td>'."\n";
									for($i = 1; $i <= $career_length_years; ++$i) {
										++$year;
										print '<td width="'.$year_cell.'"><span style="font-size: 16px;">|</span> '.$year.'</td>'."\n";
									}
									print '<td width="'.$remainder_cell.'"><span style="font-size: 16px;">|</span></td>'."\n";
?>
								</tr>
							</table>
						<br />
					</td>
				</tr>
			</table>
			<table style="width: 800px; text-align: justify;">
				<tr>
					<td class="resume_subheading" style="text-align: left;"><a href="javascript:toggle_visibility('centurylink')" style="color: black;"><img id="centurylink_img" src="../images/resume/right_arrow.png" border="0"> CenturyLink<br /> <div style="padding-left:20px; font-size:12px;">Lead IT Engineer</div></a></td><td class="resume_subheading" style="text-align: right;">May 2013 - Present</td>
				</tr>
				<tr>
					<td class="resume_detail" colspan="2">
						<div id="centurylink" style="display: none;">
							<ul>
								<li>Lead IT Engineer (AppFog)
									<ul>
										<li>Served as support and operations escalation point for the AppFog PaaS</li>
										<li>Improved customer use of the platform through clear documentation and blog posts</li>
										<li>Improved the efficiency of support and operations staff through creation of locally-used scripts and tools for better management of the platform</li>
										<li>Maintained up-to-date collection of hosts in a managed chef environment. Modified cookbooks/recipes as the needs of the platform changed.</li>
										<li>Contributed to successful delivery on three stated goals: improving platform stability, maintaining brand image, and nurturing the developer community</li>
										<li>Nurtured the growth of the support staff through ad hoc training and team troubleshooting</li>
									</ul>
								</li>
								<li>Infrastructure Systems Specialist (Previous Role)
									<ul>
										<li>Improved system and network diagnostics by deploying and administering a clustered Splunk installation for log aggregation and searching. Established relay points for several environments external to centurylinkdirect to route into the Splunk cluster.</li>
										<li>Maintained the Active Directory and DNS servers for the management network for Infrastructure, Operations, and Development teams</li>
										<li>Improved Operations monitoring capabilities by writing custom scripts to aggregate NetApp backup logs and VMware performance statistics and present them in a useful way using PowerCLI and PHP/CSS/jQuery</li>
										<li>Supported ongoing functionality of the Infrastructure and Operations teams by documenting procedures, policies, and troubleshooting steps. Documented monitoring, alert, and escalation procedures for the Operations group.</li>
										<li>Improved customer support by acting as Tier 3 support for escalation of issues from Tiers 1 and 2</li>
										<li>Promoted product success by acting as operational escalation point/subject matter expert for AppFog, an IaaS/PaaS product built on CloudFoundry aimed at developers.</li>
									</ul>
								</li>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<td class="resume_subheading" style="text-align: left;"><a href="javascript:toggle_visibility('mo_ang')" style="color: black;"><img id="mo_ang_img" src="../images/resume/right_arrow.png" border="0"> Missouri Air National Guard<br /> <div style="padding-left:20px; font-size:12px;">571st Air Force Band, 239th Combat Communications Squadron</div></a></td><td class="resume_subheading" style="text-align: right;">August 2009 - Present</td>
				</tr>
				<tr>
					<td class="resume_detail" colspan="2">
						<div id="mo_ang" style="display: none;">Current rank of Senior Airman. At Basic Military Training, appointed Element Leader, promoted to Dorm Chief over 47 trainees. Maintained orderly dormitory operations and helped flight excel (achieving three of the five possible flight distinctions) by assuming leadership role of flight activities and serving as liaison between trainees and higher command. In previous unit assignment (571st Air Force Band), actively increased unit efficiency through the introduction of a web-based database system for cataloguing the unit's music library. Currently a member of the 239 CBCS, on assignment with the MO Cyber Team (CND)<br /><br /></div>
					</td>
				</tr>
				<tr>
					<td class="resume_subheading" style="text-align: left;"><a href="javascript:toggle_visibility('caci')" style="color: black;"><img id="caci_img" src="../images/resume/right_arrow.png" border="0"> CACI, Inc.<br /> <div style="padding-left:20px; font-size:12px;">Systems Administrator</div></a></td><td class="resume_subheading" style="text-align: right;">July 2012 - May 2013</td>
				</tr>
				<tr>
					<td class="resume_detail" colspan="2">
						<div id="caci" style="display: none;">
							<ul>
								<li>Contributed to team of five Systems Administrators in effectively supporting development, pre-production, and production environments of web-based collaboration suite for military customer</li>
								<li>Administered VMware vSphere and vCenter in development environment consisting of approximately two hundred virtual servers and workstations</li>
								<li>Administered both front- and back-end of web application development on virtualized servers running RHEL 5 and Windows 2003</li>
								<li>Improved efficiency of systems administration tasks through PowerShell scripting utilizing PowerCLI</li>
								<li>Ensured mission success by supporting ESXi 4.1 host hardware, maximizing uptime given aging hardware</li>
								<li>Adapted readily to quickly changing environmental needs to maintain fruitful development efforts</li>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<td class="resume_subheading" style="text-align: left;"><a href="javascript:toggle_visibility('sluh')" style="color: black;"><img id="sluh_img" src="../images/resume/right_arrow.png" border="0"> Saint Louis University High<br /> <div style="padding-left:20px; font-size:12px;">IT Support Specialist</div></a></td><td class="resume_subheading" style="text-align: right;">November 2004 - July 2012</td>
				</tr>
				<tr>
					<td class="resume_detail" colspan="2">
						<div id="sluh" style="display: none;">
							<ul>
								<li>Ensured department success by being consistently open to professional growth and maintaining intellectual curiosity.</li>
								<li>Improved classroom technology integration through one-on-one training/in-service with teachers, as well as through effective documentation of technology at SLUH.</li>
								<li>Improved classroom communication and resource management through server setup, installation, and migration of Moodle course management server.</li>
								<li>Responded regularly to the specific needs of faculty and/or staff through the creation of custom web applications, including the development of applications:
									<ul>
										<li>Robust student attendance tracking</li>
										<li>Wrestling statistics tracking and analysis</li>
										<li>Community service tracking (included integration with FileMaker Pro database)</li>
									</ul>
								</li>
								<li>Streamlined the system deployment process through the creation of quality machine images and the development of effective workflows.</li>
								<li>Contributed to future classroom success through orientation of newly hired teachers.</li>
								<li>Maximized office efficiency by regularly organizing/reorganizing office and storage areas.</li>
								<li>Introduced electronics recycling program to reflect and enhance school's commitment to environmental stewardship.</li>
								<li>Saved the school thousands of dollars annually through the employment of open source solutions to institutional needs.</li>
								<li>Improved LAN infrastructure through competent rewiring of several areas of the building, including preparing for deployment of fiber in the building. Designed and implemented four satellite network closets.</li>
								<li>Oversaw network improvements in anticipation of the deployment of a VoIP telephone system.</li>
								<li>Impacted student learning both directly and indirectly through teaching a unit of the Web Programming course and through support of student-used technology.</li>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<td class="resume_subheading" style="text-align: left;"><a href="javascript:toggle_visibility('u-haul')" style="color: black;"><img id="u-haul_img" src="../images/resume/right_arrow.png" border="0"> U-Haul International<br /> <div style="padding-left:20px; font-size:12px;">Store Manager, Assistant Manager</div></a></td><td class="resume_subheading" style="text-align: right;">September 2002 - November 2004</td>
				</tr>
				<tr>
					<td class="resume_detail" colspan="2">
						<div id="u-haul" style="display: none;">As an Assistant Manager, consistently contributed to generating increases in sales figures over previous year levels. As a Store Manager, improved employee morale and boosted sales during the reconfiguration of an under-performing center.<br /><br /></div>
					</td>
				</tr>
				<tr>
					<td class="resume_subheading" style="text-align: left;"><a href="javascript:toggle_visibility('monsanto')" style="color: black;"><img id="monsanto_img" src="../images/resume/right_arrow.png" border="0"> Monsanto Corp.<br /> <div style="padding-left:20px; font-size:12px;">Help Desk Analyst</div></a></td><td class="resume_subheading" style="text-align: right;">January 2001 - August 2001</td>
				</tr>
				<tr>
					<td class="resume_detail" colspan="2">
						<div id="monsanto" style="display: none;">As a Help Desk Analyst, improved support pool efficiency in the Outlook/Exchange pool through strong telephone support and creation of accurate, easy-to-read documentation.<br /><br /></div>
					</td>
				</tr>
				<tr>
					<td class="resume_subheading" style="text-align: left;"><a href="javascript:toggle_visibility('truman_its')" style="color: black;"><img id="truman_its_img" src="../images/resume/right_arrow.png" border="0"> IT Services, Truman State University<br /> <div style="padding-left:20px; font-size:12px;">Web Integration, Help Desk Manager</div></a></td><td class="resume_subheading" style="text-align: right;">September 1997 - November 2000</td>
				</tr>
				<tr>
					<td class="resume_detail" colspan="2">
						<div id="truman_its" style="display: none;">As a Help Desk Analyst, helped launch the Student Help Desk and served as its manager. Led as the student manager of the Faculty/Staff Help Desk, and oversaw its growth in both staffing and hours of operation. Also wrote AIX shell scripts to improve efficiency of a variety of common processes. As Web Integration staff member, enhanced department presence by redesigning the IT Services web site, programmed a web-based employee in/out board to increase department efficiency, and improved Truman's overall web presence by designing and programming pages for a variety of campus offices and departments.<br /><br /></div>
					</td>
				</tr>
			</table>
		</div>
		<br />
		<a href="javascript:toggle_visibility('skills')" style="color: black;"><span class="resume_heading">Skills</span></a>
		<div id="skills" class="resume_division" style="display: visible;">
			<table style="padding: 0px; margin: 0px; width: 800px; text-align: justify;" cellspacing="0">
				<tr>
					<td class="resume_detail" style="width: 50%;">
						<strong>Programming</strong><br />
						Past programming projects not mentioned elsewhere include Trivia Night Manager, a scoring/statistics application, Service, a personnel scheduling/tracking system for volunteers<br />
						<ul>
							<li>PHP</li>
							<li>MySQL</li>
							<li>Perl</li>
							<li>Unix/Linux Shell Scripting</li>
							<li>HTML/CSS/JavaScript</li>
							<li>FileMaker Pro (Database &amp; PHP Integration)
						</ul>
						<div id="chart_div" style="width: 370; padding-left: 5px; padding-right: 5px;"></div>
					</td>
					<td class="resume_detail"  style="width: 50%;">
						<strong>Applications</strong><br />
						Support experience has forced experience with a wide variety of products, too numerous to list, as many are all too common, while many are very obscure. Possess expert knowledge of:<br />
						<ul>
							<li>Splunk</li>
							<li>iLife suite</li>
							<li>FileMaker Pro</li>
							<li>Adobe CS5</li>
							<li>MS Office Suite/OpenOffice.org/LibreOffice</li>
							<li>Zimbra Collaboration Suite (e-mail, calendaring, contacts, etc.)</li>
							<li>Drupal, Joomla CMS</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td class="resume_detail" style="width: 50%;">
						<strong>Operating Systems</strong><br />
						Support experience has yielded expert capability with Mac OS X and all varieties of Windows; Server administration has resulted in strong grasp of several flavors of *nix systems.<br />
						<ul>
							<li>Mac OS X (10.2 and later)</li>
							<li>Ubuntu Linux (Server &amp; Desktop Distributions)</li>
							<li>FreeBSD</li>
							<li>Microsoft Windows (All Versions)</li>
							<li>IBM AIX</li>
							<li>Solaris</li>
							<li>VMWare Virtualization</li>
						</ul>
					</td>
					<td class="resume_detail" style="width: 50%;">
						<strong>Hardware</strong><br />
						Support positions have all required ability to maintain and/or repair various types of hardware, often demanding inventive solutions.<br />
						<ul>
							<li>Apple/Mac (all Desktops &amp; Laptops from 1999 on)</li>
							<li>IBM/Compatible (all Desktops &amp; Laptops)</li>
							<li>
								Printers<br />
								All Varieties, inkjet and laser<br />
								Operational focus on HP LaserJet series
							</li>
						</ul>
					</td>
				</tr>
			</table>
		</div>
		<br />
		<a href="javascript:toggle_visibility('education')" style="color: black;"><span class="resume_heading">Education</span></a>
		<div id="education" class="resume_division" style="display: visible;">
			<table style="padding: 0px; margin: 0px; width: 800px; text-align: justify;" cellspacing="0">
				<tr>
					<td class="resume_subheading" style="text-align: left;"><br /><a href="javascript:toggle_visibility('umsl')" style="color: black;"><img id="umsl_img" src="../images/resume/right_arrow.png" border="0"> University of Missouri - St. Louis</a></td><td class="resume_subheading" style="text-align: right;">January 2008 - Present</td>
				</tr>
				<tr>
					<td class="resume_detail" colspan="2">
						<div id="umsl" style="display: none;">
							33.0 Credits Complete pursuing Bachelor of Arts in Music, anticipated graduation January 2012<br />
							3.96 Cumulative GPA (4.0 scale)
						<br /><br /></div>
					</td>
				</tr>
				<tr>
					<td class="resume_subheading" style="text-align: left;"><a href="javascript:toggle_visibility('truman')" style="color: black;"><img id="truman_img" src="../images/resume/right_arrow.png" border="0"> Truman State University</a></td><td class="resume_subheading" style="text-align: right;">August 1997 - November 2000</td>
				</tr>
				<tr>
					<td class="resume_detail" colspan="2">
						<div id="truman" style="display: none;">
							106.0 Credits completed toward Bachelor of Arts in Music - Liberal Arts<br />
							3.44 GPA (4.0 scale)
						<br /></div>
					</td>
				</tr>
			</table>
		</div>
		<br />
		<a href="javascript:toggle_visibility('honors')" style="color: black;"><span class="resume_heading">Honors</span></a>
		<div id="honors" class="resume_division" style="display: visible;">
			<table style="padding: 0px; margin: 0px; width: 800px; text-align: justify;" cellspacing="0">
				<tr>
					<td class="resume_detail" colspan="2">
							Honor Graduate, Basic Military Training, 2010<br />
							Outstanding Woodwind/Brass Musician, University of Missouri - St. Louis, 2009<br />
							Featured Soloist, Arutunian Trumpet Concerto, University of Missouri - St. Louis, 2009<br />
							UMSL Concerto Competition Finalist, University of Missouri - St. Louis, 2008, 2009<br />
						<br /><br />
					</td>
				</tr>
			</table>
		</div>
	</div>
<?php
	include("../footer.inc");
?>
</body>
</html>