<?php
	$pages_array = array();
	$this_file_path = $_SERVER["SCRIPT_NAME"];
	$file_path_parts = explode('/', $this_file_path);
	$this_file = $file_path_parts[count($file_path_parts)-1];
	$modified_date = filemtime($this_file);
?>
<html>
<link rel="stylesheet" href="/fleastack.css" type="text/css">
<head>
	<title>FleaStack - Trumpet R&eacute;sum&eacute;</title>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-26550900-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body>
	<font face="sans-serif" size="5"><b>fleastack.com</b></font>
	<br />
	<font face="sans-serif" size="2">a repository of miscellany</font>
	<br /><br /><br />
	<div class="mainbody">
		<font size="3">Trumpet R&eacute;sum&eacute;</font>
		<br /><br />
<?php
$years_experience = floor((time() - mktime(0,0,0,10,1,1988))/31557600);
?>
		I have <?=$years_experience;?> years experience on Trumpet/Flugelhorn/Cornet, with some experience on Horn as well. I have played across all styles of music, including classical, jazz, commercial, rock, musical theater, solo literature, and chamber ensembles.
		<br /><br />
		<heading>17 August 2009 - September 2013</heading>
		<br /><br />
		<div class="resume_detail">
			Member, 571st Air Force Band - Air National Guard Band of the Central States
			<br />
			Trumpet and Horn, Featured Soloist/Duetist 2012
		</div>
		<br />
		<heading>August 2010</heading>
		<br /><br />
		<div class="resume_detail">
			Horn with the USAF Band of Mid-America
		</div>
		<br />
		<heading>August 2008 - May 2010</heading>
		<br /><br />
		<div class="resume_detail">
			Principal Trumpet, University of Missouri - St. Louis Symphonic Band
		</div>
		<br />
		<heading>8 May 2009</heading>
		<br /><br />
		<div class="resume_detail">
			Named "Outstanding Woodwind/Brass Musician" by University of Missouri - St. Louis Music Department
		</div>
		<br />
		<heading>5 December 2008</heading>
		<br /><br />
		<div class="resume_detail">
			Trumpet III (substitute), St. Louis Philharmonic Orchestra at the Blanche M. Touhill PAC at UMSL
			<br />
			Holiday Pops Concert
		</div>
		<br />
		<heading>21 November 2008</heading>
		<br /><br />
		<div class="resume_detail">
			Trumpet III (substitute), St. Louis Philharmonic Orchestra at the William D. Purser Center at Logan University
			<br />
			Symphony No. 3 in A minor, Op. 44 - Sergei Rachmaninoff
		</div>
		<br />
		<heading>27 September 2008</heading>
		<br /><br />
		<div class="resume_detail">
			Trumpet III (substitute), St. Louis Philharmonic Orchestra at the Fabulous Fox Theater
			<br />
			Pops Concert
		</div>
		<br />
		<heading>20 September 2008</heading>
		<br /><br />
		<div class="resume_detail">
			Trumpeter, St. Louis Craft Alliance
		</div>
	</div>
<?php
	include("../footer.inc");
?>
</body>
</html>